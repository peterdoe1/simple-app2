defmodule Pd.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      PdWeb.Telemetry,
      Pd.Repo,
      {DNSCluster, query: Application.get_env(:pd, :dns_cluster_query) || :ignore},
      {Phoenix.PubSub, name: Pd.PubSub},
      # Start the Finch HTTP client for sending emails
      {Finch, name: Pd.Finch},
      # Start a worker by calling: Pd.Worker.start_link(arg)
      # {Pd.Worker, arg},
      # Start to serve requests, typically the last entry
      PdWeb.Endpoint
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Pd.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    PdWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
