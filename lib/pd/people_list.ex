defmodule Pd.PeopleList do
  import Ecto.Query

  def index(params) do
    Pd.Person
    |> from(as: :people)
    |> filter_first_name(params["first_name"])
    |> filter_last_name(params["last_name"])
    |> filter_sex(params["sex"])
    |> filter_date_of_birth_start(params["date_of_birth_start"])
    |> filter_date_of_birth_end(params["date_of_birth_end"])
    |> maybe_order_by(params["sort_by"])
    |> Pd.Repo.all()
  end

  defp filter_first_name(query, value) when byte_size(value) > 0 do
    query |> where([people: p], ilike(p.first_name, ^"%#{value}%"))
  end

  defp filter_first_name(query, _), do: query

  defp filter_last_name(query, value) when byte_size(value) > 0 do
    query |> where([people: p], ilike(p.last_name, ^"%#{value}%"))
  end

  defp filter_last_name(query, _), do: query

  defp filter_sex(query, "male") do
    query |> where([people: p], p.sex == true)
  end

  defp filter_sex(query, "female") do
    query |> where([people: p], p.sex == false)
  end

  defp filter_sex(query, _), do: query

  defp filter_date_of_birth_start(query, value) when byte_size(value) > 0 do
    case Date.from_iso8601(value) do
      {:ok, date} -> query |> where([people: p], p.date_of_birth >= ^date)
      _ -> query
    end
  end

  defp filter_date_of_birth_start(query, _), do: query

  defp filter_date_of_birth_end(query, value) when byte_size(value) > 0 do
    case Date.from_iso8601(value) do
      {:ok, date} -> query |> where([people: p], p.date_of_birth <= ^date)
      _ -> query
    end
  end

  defp filter_date_of_birth_end(query, _), do: query

  # NOTE: String to atom is not safe, list should be filtered by allowed values
  # TODO: Could add option to change sort order
  defp maybe_order_by(query, value) when byte_size(value) > 0 do
    field_name = String.to_atom(value)
    query |> order_by([people: p], asc: field(p, ^field_name))
  end

  defp maybe_order_by(query, _value) do
    query
  end
end
