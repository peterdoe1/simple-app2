defmodule Pd.Actions.Person do
  def update(person, params) do
    Pd.Person.update_changeset(person, params) |> Pd.Repo.update()
  end

  def create(params) do
    Pd.Person.create_changeset(params) |> Pd.Repo.insert()
  end
end
