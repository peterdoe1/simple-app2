defmodule Pd.Person do
  use Ecto.Schema
  import Ecto.Changeset

  @params [:first_name, :last_name, :date_of_birth, :sex]

  schema "people" do
    field :first_name, :string
    field :last_name, :string
    field :date_of_birth, :date
    field :sex, :boolean

    timestamps()
  end

  def create_changeset(params \\ %{}) do
    %__MODULE__{}
    |> cast(params, @params)
    |> validate_required(@params)
  end

  def update_changeset(%__MODULE__{} = model, params \\ %{}) do
    model
    |> cast(params, @params)
    |> validate_required(@params)
  end
end
