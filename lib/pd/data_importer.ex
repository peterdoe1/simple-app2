defmodule Pd.DataImporter do
  @female_first_names "https://api.dane.gov.pl/1.4/resources/54110,lista-imion-zenskich-w-rejestrze-pesel-stan-na-19012024-imie-pierwsze/data?per_page=100"
  @female_last_names "https://api.dane.gov.pl/1.4/resources/54098,nazwiska-zenskie-stan-na-2024-01-19/data?per_page=100"

  @male_first_names "https://api.dane.gov.pl/1.4/resources/54109,lista-imion-meskich-w-rejestrze-pesel-stan-na-19012023-imie-pierwsze/data?per_page=100"
  @male_last_names "https://api.dane.gov.pl/1.4/resources/54097,nazwiska-meskie-stan-na-2024-01-19/data?per_page=100"

  @max_date_of_birth "2023-12-31 00:00:00Z"

  def perform do
    male_names = fetch_data!(@male_first_names)
    male_last_names = fetch_data!(@male_last_names)
    female_names = fetch_data!(@female_first_names)
    female_last_names = fetch_data!(@female_last_names)

    1..100
    |> Enum.to_list()
    |> Enum.map(fn _ ->
      Enum.random(["male", "female"])
    end)
    |> Enum.each(fn sex ->
      if sex == "male" do
        create_person(male_names, male_last_names, true)
      else
        create_person(female_names, female_last_names, false)
      end
    end)
  end

  defp fetch_data!(url) do
    %HTTPoison.Response{status_code: 200, body: body} = HTTPoison.get!(url)

    body
    |> Poison.decode!()
    |> Map.get("data")
    |> Enum.map(fn %{"attributes" => %{"col1" => %{"val" => val}}} ->
      val
    end)
  end

  defp create_person(names, last_names, sex) do
    Pd.Person.create_changeset(%{
      first_name: Enum.random(names),
      last_name: Enum.random(last_names),
      sex: sex,
      date_of_birth: random_day_of_birth!()
    })
    |> Pd.Repo.insert!()
  end

  # Because range starts at 1970-01-01 we can rand between 0 and given timestamp using :rand.uniform
  defp random_day_of_birth! do
    {:ok, max_datetime, _} = DateTime.from_iso8601(@max_date_of_birth)
    max_datetime |> DateTime.to_unix() |> :rand.uniform() |> DateTime.from_unix!()
  end
end
