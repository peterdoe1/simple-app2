defmodule PdWeb.PeopleController do
  use PdWeb, :controller

  @query_params [
    "first_name",
    "last_name",
    "sex",
    "date_of_birth_start",
    "date_of_birth_end",
    "sort_by"
  ]
  def index(conn, params) do
    people = Pd.PeopleList.index(params)
    render(conn, :index, people: people, current_params: Map.take(params, @query_params))
  end

  def new(conn, _params) do
    render(conn, :new)
  end

  def show(conn, params) do
    person = Pd.Repo.get!(Pd.Person, params["id"])
    render(conn, :show, person: person)
  end

  def edit(conn, params) do
    person = Pd.Repo.get!(Pd.Person, params["id"])
    render(conn, :edit, person: person)
  end

  def create(conn, params) do
    case Pd.Actions.Person.create(params) do
      {:ok, person} ->
        redirect(conn, to: "/people/#{person.id}")

      {:error, _changeset} ->
        # TODO: Here we should actually handle errors and maybe add flash message
        redirect(conn, to: "/people/new")
    end
  end

  def update(conn, params) do
    person = Pd.Repo.get!(Pd.Person, params["id"])

    case Pd.Actions.Person.update(person, params) do
      {:ok, person} ->
        redirect(conn, to: "/people/#{person.id}")

      {:error, _changeset} ->
        # TODO: Here we should actually handle errors and maybe add flash message
        redirect(conn, to: "/people/#{person.id}/edit")
    end
  end

  def delete(conn, params) do
    person = Pd.Repo.get!(Pd.Person, params["id"])

    case Pd.Repo.delete(person) do
      {:ok, _deleted_person} ->
        redirect(conn, to: "/")

      {:error, _error} ->
        # TODO: Maybe return flash message?
        redirect(conn, to: "/people/#{person.id}")
    end
  end
end
