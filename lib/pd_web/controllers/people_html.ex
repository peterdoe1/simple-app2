defmodule PdWeb.PeopleHTML do
  use PdWeb, :html

  embed_templates "people_html/*"
end
