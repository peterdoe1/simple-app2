defmodule PdWeb.PageHTML do
  use PdWeb, :html

  embed_templates "page_html/*"
end
