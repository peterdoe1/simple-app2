defmodule PdWeb.Layouts do
  use PdWeb, :html

  embed_templates "layouts/*"
end
