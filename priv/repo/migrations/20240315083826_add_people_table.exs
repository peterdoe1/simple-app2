defmodule Pd.Repo.Migrations.AddPeopleTable do
  use Ecto.Migration

  def up do
    create table("people") do
      add :first_name, :string, size: 64
      add :last_name, :string, size: 64
      add :date_of_birth, :date
      add :sex, :boolean

      timestamps()
    end

    create index("people", [:first_name])
    create index("people", [:last_name])
    create index("people", [:date_of_birth])
    create index("people", [:sex])
  end

  def down do
    drop table("people")
  end
end
